<?php 
 
 require_once 'conn.php';
 //echo"api connected";
 $response = array();
 
 if(isset($_GET['apicall'])){
 
 switch($_GET['apicall']){
 
  case 'signup':
         
  if(isTheseParametersAvailable(array('mobileno','fname','lname','email','type','password'))){
  $mobileno = $_POST['mobileno']; 
      $fname= $_POST['fname']; 
      $lname=$_POST['lname']; 
        $email = $_POST['email']; 
      $type=$_POST['type'];
  $password = ($_POST['password']);
  
 
  $stmt = $conn->prepare("SELECT mobileno FROM users WHERE mobileno = ?");
  $stmt->bind_param("s", $mobileno);
  $stmt->execute();
  $stmt->store_result();
 
  if($stmt->num_rows > 0){
  $response['error'] = true;
  $response['message'] = 'User already registered';
  $stmt->close();
  }else{
  $stmt = $conn->prepare("INSERT INTO users (mobileno,fname,lname,email,password,type ) VALUES (?, ?, ?, ?,?,?)");
  
      $stmt->bind_param("ssssss", $mobileno,$fname,$lname,$email, $password,$type);
 
  if($stmt->execute()){
  $stmt = $conn->prepare("SELECT mobileno,fname,email,type FROM users WHERE mobileno = ?"); 
  $stmt->bind_param("s",$mobileno);
  $stmt->execute();
  $stmt->bind_result($mobileno,$fname,$email,$type);
  $stmt->fetch();
 
  $user = array(
  'mobileno'=>$mobileno,
  'fname'=>$fname, 
    'email'=>$email,
      'type'=>$type
  );
 
  $stmt->close();
 
  $response['error'] = false; 
  $response['message'] = 'User registered successfully'; 
  $response['user'] = $user; 
  }
  }
 
  }else{
  $response['error'] = true; 
  $response['message'] = 'required parameters are not available'; 
  }
 
  break; 
         
         
         
 case 'login':
if(isTheseParametersAvailable(array('mobileno', 'password'))){
    
 //getting values 
 $mobileno = $_POST['mobileno'];
 $password = $_POST['password']; 
 
 
 //creating the query 
 $stmt = $conn->prepare("SELECT mobileno, fname, email, type FROM users WHERE mobileno = ? AND password = ?");
 $stmt->bind_param("ss",$mobileno, $password);
 
 $stmt->execute();
 
 $stmt->store_result();
 
 //if the user exist with given credentials 
 if($stmt->num_rows > 0){
 
 $stmt->bind_result($mobileno,$fname, $email,$type);
 $stmt->fetch();
 
 $user = array(
'mobileno'=>$mobileno,
  'fname'=>$fname,'email'=>$email,'type'=>$type
 );
 
 $response['error'] = false; 
 $response['message'] = 'Login successfull'; 
 $response['user'] = $user; 
 } else{
 //if the user not found 
 $response['error'] = false; 
 $response['message'] = 'Invalid username or password';
 }
 }else{
     echo"parameter missing";
 }
 break; 
 case 'profile_edit':
 if(isTheseParametersAvailable(array('mobileno'))){
    
 //getting values 
 $mobileno = $_POST['mobileno'];
 
 $stmt = $conn->prepare("SELECT fname,lname FROM users WHERE mobileno = ?");
 $stmt->bind_param("s",$mobileno);
 $stmt->execute();

 $stmt->store_result();
 
 //if the user exist with given credentials 
 if($stmt->num_rows > 0){
 
 $stmt->bind_result($fname,$lname);
 $stmt->fetch();
 
 $user = array(
  'fname'=>$fname,'lname'=>$lname
 );
 
 $response['error'] = false; 
 $response['message'] = 'Data fetched';
  $response['user'] = $user; 
 } else{
 //if the user not found 
 $response['error'] = false; 
 $response['message'] = 'Invalid user fetching fail';
 }
 }else{
     echo"parameter missing";
 }
 break;
 
 case 'profile_update':
 
 if(isTheseParametersAvailable(array('mobileno','fname','lname','opassword','npassword'))){
  $mobileno = $_POST['mobileno']; 
  $fname= $_POST['fname']; 
  $lname=$_POST['lname']; 
       
  $opassword = ($_POST['opassword']);
  $npassword=($_POST['npassword']);
  
  
  $stmt = $conn->prepare("SELECT mobileno FROM users WHERE mobileno = ? AND password=?");
  $stmt->bind_param("ss", $mobileno,$opassword);
  $stmt->execute();
  $stmt->store_result();
  if($stmt->num_rows > 0){
	  
  $stmt = $conn->prepare("UPDATE users SET fname=?,lname=?,password=? WHERE mobileno=? AND password=?");
  $stmt->bind_param("sssss",$fname,$lname,$npassword,$mobileno,$opassword);
  $stmt->execute();
//  $stmt->store_result();
  
 $stmt = $conn->prepare("SELECT mobileno, fname, email, type FROM users WHERE mobileno = ? AND password = ?");
 $stmt->bind_param("ss",$mobileno, $npassword);
 $stmt->execute();
 
 $stmt->store_result();
 if($stmt->num_rows > 0){ 
 $stmt->bind_result($mobileno,$fname, $email,$type);
 $stmt->fetch();
  $user = array(
'mobileno'=>$mobileno,
  'fname'=>$fname,'email'=>$email,'type'=>$type
 );
 $response['error'] = false;
  $response['message'] = 'Update done';
  $response['user'] = $user;
 }
 else{
   $response['error'] = true;
  $response['message'] = 'Some error occured';
}
  $stmt->close();
   }
  
  else{
   $response['error'] = true; 
  $response['message'] = 'old password might be wrong'; 
   }
  }
  else{
  $response['error'] = true; 
  $response['message'] = 'required parameters are not available'; 
  }
 break;
 
 
case'feedback':
if(isTheseParametersAvailable(array('rating','mobile'))){
  $rating= $_POST['rating'];
  $mobile=$_POST['mobile'];
    $stmt = $conn->prepare("UPDATE users SET feedback = '?' where mobileno = '?' ");
  $stmt->bind_param("ss",$rating,$mobile);
  
 if($stmt->execute()){
 $response['error'] = false;
  $response['message'] = 'Thank You For Rating us!!';
 }else{
 $response['error'] = true;
  $response['message'] = 'Some error occured please Try again!!';
 }

}
else{
 $response['error'] = true; 
  $response['message'] = 'required parameters are not available'; 
}
break;
 
 
 default: 
 $response['error'] = true; 
 $response['message'] = 'Invalid Operation Called';
 
 }
 
 }else{
 $response['error'] = true; 
 $response['message'] = 'Invalid API Call';
 }
 
 
echo json_encode($response);
 
 function isTheseParametersAvailable($params){
 
 foreach($params as $param){
 if(!isset($_POST[$param])){
 return false; 
 }
 }
 return true; 
 }
