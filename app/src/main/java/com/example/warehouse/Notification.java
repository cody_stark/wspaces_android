package com.example.warehouse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.login_reg_pannels.LOGIN;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Notification extends AppCompatActivity {
    String urll;
    ListView listView;
    User user;
    ArrayAdapter adapter;
    ArrayList<String> messageArray = new ArrayList<String>();

    //    String[] messageArray;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        user = SharedPref.getInstance(getApplicationContext()).getUser();

        adapter = new ArrayAdapter(getApplicationContext(),
                R.layout.activity_notification, messageArray);

        listView = findViewById(R.id.listview);


        if (!SharedPref.getInstance(getApplicationContext()).isLoggedIn()) {

            Toast.makeText(this, "Not Logged in", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), LOGIN.class);
            startActivity(intent);
            finish();
        } else {
            if (user.getType() == "owner")
                urll = urls.URL_OWNER_NOTIFIACTION;
            else
                urll = urls.URL_USER_NOTIFIACTION;

        }


    }

    public void sendNotification(View view) {

    }

    public void backend() {
        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, urll, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                if (!response.contains("error")) {
                try {
                    Log.d("main", "resp" + response);
                    JSONArray obj = new JSONArray(response);

                    for (int i = 0; i < obj.length(); i++) {
                        JSONObject jsonObject = obj.getJSONObject(i);
                        messageArray.add(jsonObject.getString("message"));
                    }
                    listView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
//                else {
//                    Toast.makeText(getApplicationContext(), "Something happend", Toast.LENGTH_SHORT).show();
//                }
//            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response " + error, Toast.LENGTH_LONG).show();
                Log.d("main", "asds");
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", String.valueOf(user.getMobileno()));
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest1);
    }
}
