package com.example.warehouse.warehouse_dao;

public class User {
    long mobileno;
    private String Username, email, type;

    public User(long mobileno, String username, String email, String type) {
        this.mobileno = mobileno;
        Username = username;
        this.email = email;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User(String username, String email, long mobileno) {
        Username = username;
        this.email = email;
        this.mobileno = mobileno;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getMobileno() {
        return mobileno;
    }

    public void setMobileno(long mobileno) {
        this.mobileno = mobileno;
    }


}