package com.example.warehouse.warehouse_dao;


import java.io.Serializable;

public class w_details implements Serializable {

    String regno;
    String ware_name;
    String o_name;
    String mobileno;
    String email;
    String type;
    String storeage;
    String cost;
    String address;
    String pincode;
    String ratting;

    public w_details(String regno, String ware_name, String o_name, String mobileno, String email, String type, String storeage, String cost, String address, String pincode, String ratting) {
        this.regno = regno;
        this.ware_name = ware_name;
        this.o_name = o_name;
        this.mobileno = mobileno;
        this.email = email;
        this.type = type;
        this.storeage = storeage;
        this.cost = cost;
        this.address = address;
        this.pincode = pincode;
        this.ratting = ratting;
    }

    public String getRatting() {
        return ratting;
    }

    public void setRatting(String ratting) {
        this.ratting = ratting;
    }

    public w_details(String regno, String ware_name, String o_name, String mobileno, String email, String type, String storeage, String cost, String address, String pincode) {

        this.regno = regno;
        this.ware_name = ware_name;
        this.o_name = o_name;
        this.mobileno = mobileno;
        this.email = email;
        this.type = type;
        this.storeage = storeage;
        this.cost = cost;
        this.address = address;
        this.pincode = pincode;
    }

    public w_details() {
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getWare_name() {
        return ware_name;
    }

    public void setWare_name(String ware_name) {
        this.ware_name = ware_name;
    }

    public String getO_name() {
        return o_name;
    }

    public void setO_name(String o_name) {
        this.o_name = o_name;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStoreage() {
        return storeage;
    }

    public void setStoreage(String storeage) {
        this.storeage = storeage;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }


}
