package com.example.warehouse.warehouse_dao;


public class warehouse {
    private String name;
    private String address;
    private String owner_name;
    private String ratting;

    public warehouse(String name, String address, String owner_name, String ratting) {
        this.name = name;
        this.address = address;
        this.owner_name = owner_name;
        this.ratting = ratting;
    }

    public String getRatting() {
        return ratting;
    }

    public void setRatting(String ratting) {
        this.ratting = ratting;
    }

//    public warehouse(String name, String address, String owner_name) {
//        this.name = name;
//        this.address = address;
//        this.owner_name = owner_name;
//    }

    public warehouse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }
}
