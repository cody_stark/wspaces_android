package com.example.warehouse.login_reg_pannels;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.R;
import com.example.warehouse.SharedPref;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//import android.widget.RadioButton;

public class user_reg extends AppCompatActivity {

    EditText firstmame;
    EditText lastname;
    EditText number;
    EditText password;
    EditText cpassword;
    EditText emailadd;
    RadioButton rb1, rb2;
    RadioGroup rdg;
    String first;
    String last;
    String numb;
    String passw;
    String passwc;
    String type;
    String email;
    ProgressBar progressBar;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_registration);
        firstmame = findViewById(R.id.FirstName);
        lastname = findViewById(R.id.LastName);
        number = findViewById(R.id.Mobile);
        password = findViewById(R.id.Password);
        cpassword = findViewById(R.id.ConfirmPassword);
        rb1 = findViewById(R.id.radioButton1);
        rb2 = findViewById(R.id.radioButton2);
        rdg = findViewById(R.id.rdg);
        emailadd = findViewById(R.id.email);
        progressBar = findViewById(R.id.progressBar2);
        button = findViewById(R.id.button2);
    }

    public void submit(View view) {


        first = firstmame.getText().toString();
        last = lastname.getText().toString();
        numb = number.getText().toString();
        passw = password.getText().toString();
        passwc = cpassword.getText().toString();
        email = emailadd.getText().toString();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (first.isEmpty()) {
            firstmame.setError("please insert");
            firstmame.requestFocus();
        } else if (last.isEmpty()) {
            lastname.setError("please insert");
            lastname.requestFocus();

        } else if (numb.isEmpty() || numb.length() != 10) {
            number.setError("please insert");
            number.requestFocus();

        } else if (passw.isEmpty()) {
            password.setError("please insert");
            password.requestFocus();

        } else if (passwc.isEmpty()) {
            cpassword.setError("please insert");
            cpassword.requestFocus();

        } else if (!rb1.isChecked() && !rb2.isChecked()) {


            Toast.makeText(this, "please select owner or farmer", Toast.LENGTH_LONG).show();
            rdg.requestFocus();
        } else {
            if (!passw.equals(passwc)) {
                cpassword.setError("incorrect");
                cpassword.requestFocus();
            } else {


                if (rdg.getCheckedRadioButtonId() == R.id.radioButton2) {
                    type = "owner";
                } else {
                    type = "user";
                }

                progressBar.setVisibility(View.VISIBLE);
//
                backend();
                // String type = "login";
                // Backgroundworker backgroundWorker = new Backgroundworker(this);
                // backgroundWorker.execute(type, first, last, numb, passw, passwc);
                //  Toast.makeText(user_reg.this, "data entered", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(user_reg.this, Main22Activity.class);
//            startActivity(intent);
            }
        }
    }

    public void backend() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_REGISTER
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.INVISIBLE);

                try {

//                    Toast.makeText(getApplicationContext(), "REGISTRATION DONE", Toast.LENGTH_SHORT).show();

                    //converting response to json object
                    JSONObject obj = new JSONObject(response);

                    //if no error in response
                    if (!obj.getBoolean("error")) {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        //getting the user from the response
                        JSONObject userJson = obj.getJSONObject("user");

                        //creating a new user object
                        User user = new User(
                                Long.parseLong(userJson.getString("mobileno").trim().toLowerCase()),
                                userJson.getString("fname").trim().toLowerCase(),
                                userJson.getString("email").trim().toLowerCase(),
                                userJson.getString("type").trim().toLowerCase()

                        );

                        SharedPref.getInstance(getApplicationContext()).userLogin(user);

                        Intent intent = new Intent(getApplicationContext(), LOGIN.class);
                        startActivity(intent);
                        finish();
                        Log.wtf("user reg", "onResponse: " + response);
                    } else {

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_LONG).show();
                error.printStackTrace();
                Log.i("erroe", "error is" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("mobileno", numb);
                params.put("fname", first);
                params.put("lname", last);
                params.put("email", email);
                params.put("type", type);
                params.put("password", passwc);

                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

}

