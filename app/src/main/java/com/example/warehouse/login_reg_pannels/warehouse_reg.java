package com.example.warehouse.login_reg_pannels;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.R;
import com.example.warehouse.SharedPref;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;
import com.example.warehouse.warehouse_owner_nav;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class warehouse_reg extends AppCompatActivity {
    EditText mobile, warehouse_name, owner_name, address, pincode, emailaddress, space, costet, type1;
    String phone, wname, oname, add, pincodes, email, spaces, cost, type;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.warehouseregistration);
        user = SharedPref.getInstance(getApplicationContext()).getUser();

        findview();
    }

    public void findview() {
        mobile = findViewById(R.id.Mobile);
        warehouse_name = findViewById(R.id.warehouse);
        owner_name = findViewById(R.id.person);
        address = findViewById(R.id.Address);
        pincode = findViewById(R.id.Pincode);
        emailaddress = findViewById(R.id.emailid);
        space = findViewById(R.id.capacity);
        costet = findViewById(R.id.cost1);
        type1 = findViewById(R.id.type1);
        mobile.setText("" + user.getMobileno());
        mobile.setEnabled(false);

    }

    public void submit(View view) {

        phone = "" + user.getMobileno();
        wname = warehouse_name.getText().toString();
        oname = owner_name.getText().toString();
        add = address.getText().toString();
        pincodes = pincode.getText().toString();
        email = emailaddress.getText().toString();
        spaces = space.getText().toString();
        cost = costet.getText().toString();
        type = type1.getText().toString();

        if (phone.isEmpty() || wname.isEmpty() || oname.isEmpty() || add.isEmpty() || pincodes.isEmpty() || email.isEmpty() || spaces.isEmpty() || cost.isEmpty() || type.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter the data", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("values", "valll" + phone + wname + oname + add + pincodes + email + spaces + cost + type);
            backend();
        }
    }

    public void backend() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_OWNER_WARELIST_REGISTER
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("responce", "resp ise in warehouse:" + response);
                try {
                    JSONObject obj = new JSONObject(response);

                    //if no error in response
                    if (!obj.getBoolean("error")) {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), warehouse_owner_nav.class);
                        startActivity(intent);
                        finish();
                    } else {

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_LONG).show();
                error.printStackTrace();
                Log.i("error", "error is" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", phone.trim());
                params.put("oname", oname.trim());
                params.put("warehousename", wname.trim());
                params.put("email", email.trim());
                params.put("type", type.trim());
                params.put("capacity", spaces.trim());
                params.put("cost", cost.trim());
                params.put("address", add.trim());
                params.put("pincode", pincodes.trim());
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
