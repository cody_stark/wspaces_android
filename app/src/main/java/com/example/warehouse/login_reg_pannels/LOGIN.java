package com.example.warehouse.login_reg_pannels;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.R;
import com.example.warehouse.SharedPref;
import com.example.warehouse.User_nav;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;
import com.example.warehouse.warehouse_owner_nav;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LOGIN extends AppCompatActivity {
    ImageView Logo;

    Button Login;
    EditText username, pass;
    TextView reg;
    RelativeLayout splashscrn, main;
    LinearLayout Loginscrn;
    ProgressBar progressBar;
    String uname, password;
//    JSONObject json;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findview();
        getSupportActionBar().hide(); //hide the title bar


        if (!SharedPref.getInstance(getApplicationContext()).isLoggedIn()) {
//            Animation();
            Toast.makeText(getApplicationContext(), "LOGIN CALLED", Toast.LENGTH_LONG).show();
        } else {
            User user1 = SharedPref.getInstance(getApplicationContext()).getUser();
            Log.i("Share pref", "user:" + user1.getType());
            intentcall(user1.getType());
        }
    }

    public void findview() {
        main = findViewById(R.id.mainsplash);
        Logo = findViewById(R.id.LOGO);
        progressBar = findViewById(R.id.progressBar);
        Login = findViewById(R.id.BTN_LOGIN);
        username = findViewById(R.id.etxt_Username);
        pass = findViewById(R.id.etxt_password);
        reg = findViewById(R.id.BTN_newuser);
        splashscrn = findViewById(R.id.splashscrn);
        Loginscrn = findViewById(R.id.Loginscrn);
    }

    public void Login(View view) {

        uname = username.getText().toString();
        password = pass.getText().toString();

        if (uname.isEmpty()) {
            username.setError("Please enter username ");
            username.requestFocus();
        } else if (password.isEmpty()) {

            pass.setError("Please enter Password");
            pass.requestFocus();

        } else {
            username.onEditorAction(EditorInfo.IME_ACTION_DONE);

            pass.onEditorAction(EditorInfo.IME_ACTION_DONE);

            progressBar.setVisibility(View.VISIBLE);
            backend();
        }

    }


    public void Register(View view) {
        Intent intent = new Intent(getApplicationContext(), user_reg.class);
        startActivity(intent);

        Toast.makeText(this, "Registraion", Toast.LENGTH_SHORT).show();

      //  finish();
    }


    public void Animation() {
//todo:"animation,transition"
        splashscrn.animate().scaleXBy(0.5f).setDuration(10);
        splashscrn.animate().scaleYBy(0.5f).setDuration(10);
        splashscrn.setTranslationY(5f);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                main.setBackground(getDrawable(R.drawable.back));
//            }
//        }


    }

    public boolean backend() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_LOGIN
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progressBar.setVisibility(View.INVISIBLE);

                    //converting response to json object
                    JSONObject obj = new JSONObject(response);

                    //if no error in response
                    if (!obj.getBoolean("error")) {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        //getting the user from the response
                        JSONObject userJson = obj.getJSONObject("user");

                        //creating a new user object
                        User user = new User(
                                Long.parseLong(userJson.getString("mobileno").toLowerCase()),
                                userJson.getString("fname").trim().toLowerCase(),
                                userJson.getString("email").trim().toLowerCase(),
                                userJson.getString("type").trim().toLowerCase()
                        );

                        SharedPref.getInstance(getApplicationContext()).userLogin(user);
//                        Toast.makeText(getApplicationContext(), obj.getString("TYPE")+userJson.getString("type"), Toast.LENGTH_SHORT).show();
//                        intentcall("user");
                        intentcall(userJson.getString("type").trim().toLowerCase());


                    } else {

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("mobileno", uname);
                params.put("password", password);
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

        return true;
    }

    public void intentcall(String type) {
        Log.i("TYPE", "TYPE IS:" + type);
        if (type.equals("owner")) {
            Intent intent = new Intent(getApplicationContext(), warehouse_owner_nav.class);
            startActivity(intent);
            finish();
        } else if (type.equals("user")) {
            Intent intent = new Intent(getApplicationContext(), User_nav.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "null called", Toast.LENGTH_LONG).show();
        }
    }


}

//api 25 ke liye
//user name:9001444138,pass:owner
//700506995 pass:user