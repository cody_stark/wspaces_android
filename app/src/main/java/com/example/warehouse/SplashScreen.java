package com.example.warehouse;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.login_reg_pannels.LOGIN;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;

public class SplashScreen extends AppCompatActivity {

    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();
        progressBar = findViewById(R.id.progressBar);
        backend();
//        thread();
    }

    public void thread() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(getApplicationContext(), "RESPONSE done", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), LOGIN.class);
                startActivity(intent);
                finish();

            }
        }, 2000);
    }

    public void backend() {
//        Toast.makeText(getApplicationContext(), "Backend callled", Toast.LENGTH_SHORT).show();

        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, urls.URL_CONN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.contains("error")) {
//                    Toast.makeText(getApplicationContext(), "no error in RESPONSE", Toast.LENGTH_SHORT).show();
                    thread();

                } else {
                    Toast.makeText(getApplicationContext(), "Something happend", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response " + error, Toast.LENGTH_LONG).show();
                Log.d("main", "asds");
                error.printStackTrace();
                progressBar.setVisibility(View.GONE);

            }
        });
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest1);
    }

}

