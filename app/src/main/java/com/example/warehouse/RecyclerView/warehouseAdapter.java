package com.example.warehouse.RecyclerView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.warehouse.R;
import com.example.warehouse.warehouse_dao.warehouse;

import java.util.List;

public class warehouseAdapter extends RecyclerView.Adapter<warehouseAdapter.MyViewList> {
    private List<warehouse> WarehouseList;


    public warehouseAdapter(List warehouseList) {
        WarehouseList = warehouseList;


    }

    @NonNull
    @Override
    public MyViewList onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.warehouse_search_list, viewGroup, false);

        return new MyViewList(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewList myViewList, int i) {
        warehouse whouse = WarehouseList.get(i);

        myViewList.wname.setText(whouse.getName());
        myViewList.oname.setText(whouse.getOwner_name());
        myViewList.add.setText(whouse.getAddress());
        myViewList.ratting.setText(whouse.getRatting());
    }

    @Override
    public int getItemCount() {
        return WarehouseList.size();
    }

    public void updateList(List<warehouse> list) {
        WarehouseList = list;
        notifyDataSetChanged();
    }

    public class MyViewList extends RecyclerView.ViewHolder {
        public TextView wname, oname, add, ratting;

        public MyViewList(@NonNull View itemView) {
            super(itemView);
            wname = itemView.findViewById(R.id.wname);
            add = itemView.findViewById(R.id.add);
            oname = itemView.findViewById(R.id.oname);
            ratting = itemView.findViewById(R.id.ratting);
        }
    }

}
