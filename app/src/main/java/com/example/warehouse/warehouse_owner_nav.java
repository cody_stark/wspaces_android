package com.example.warehouse;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.RecyclerView.RecyclerTouchListner;
import com.example.warehouse.RecyclerView.warehouseAdapter;
import com.example.warehouse.content_nav.contactus;
import com.example.warehouse.content_nav.editprofile;
import com.example.warehouse.content_nav.explore;
import com.example.warehouse.content_nav.warehouse_details;
import com.example.warehouse.content_nav.weather;
import com.example.warehouse.login_reg_pannels.LOGIN;
import com.example.warehouse.login_reg_pannels.warehouse_reg;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;
import com.example.warehouse.warehouse_dao.w_details;
import com.example.warehouse.warehouse_dao.warehouse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class warehouse_owner_nav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @SuppressLint("StaticFieldLeak")
    public static Context context;
    User user;
    private List<warehouse> warehouseList = new ArrayList<>();
    private List<w_details> listt = new ArrayList<>();
    private RecyclerView recyclerView;
    private warehouseAdapter mAdapter;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warehouse_owner_nav);

        user = SharedPref.getInstance(getApplicationContext()).getUser();

        if (!SharedPref.getInstance(getApplicationContext()).isLoggedIn()) {
            Toast.makeText(this, "Not Logged in", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LOGIN.class);
            startActivity(intent);
            finish();
        } else {
            recyclerv();
            notification();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), warehouse_reg.class);
                startActivity(i);
                //  finish();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    // @Override
    // public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    // getMenuInflater().inflate(R.menu.warehouse_owner_nav, menu);
    //   return true;
    // }

    // @Override
    //public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    //  int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    //   if (id == R.id.action_settings) {
    //    return true;
    //  }

    // return super.onOptionsItemSelected(item);
    // }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent i = new Intent(warehouse_owner_nav.this, editprofile.class);
            startActivity(i);
        } else if (id == R.id.nav_contact) {
            Intent i = new Intent(getApplicationContext(), contactus.class);
            startActivity(i);

        } else if (id == R.id.nav_order) {
            Intent i = new Intent(getApplicationContext(), explore.class);
            startActivity(i);
        }
// else if (id == R.id.nav_notification) {
//            Intent i = new Intent(getApplicationContext(), Notification.class);
//            startActivity(i);
//        }
        else if (id == R.id.wheather) {
            Intent i = new Intent(getApplicationContext(), weather.class);
            startActivity(i);
        } else if (id == R.id.Rating2) {


 /*           mainLayout.addView(view);
            btn=findViewById(R.id.action);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rate = ratingBar.getRating();

//        backend();

                    Toast.makeText(getApplicationContext(),""+rate,Toast.LENGTH_LONG).show();
                }
            });*/

            Intent i = new Intent(getApplicationContext(), Ratting.class);
            startActivity(i);
        } else if (id == R.id.Register) {
            Intent i = new Intent(getApplicationContext(), warehouse_reg.class);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            SharedPref.getInstance(getApplicationContext()).logout();

            Toast.makeText(this, "Logged Out successfull", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), LOGIN.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void backend() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_OWNER_WARELIST

                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray obj = new JSONArray(response);


                    for (int i = 0; i < obj.length(); i++) {
                        JSONObject jsonObject = obj.getJSONObject(i);

                        warehouse warehouse = new warehouse();
                        w_details w_details = new w_details();

                        warehouse.setName(jsonObject.getString("warehousename"));
                        warehouse.setOwner_name(jsonObject.getString("ownername"));
                        warehouse.setAddress(jsonObject.getString("address"));
                        warehouse.setRatting(jsonObject.getString("ratting"));

                        w_details.setRegno(jsonObject.getString("registration"));
                        w_details.setWare_name(jsonObject.getString("warehousename"));
                        w_details.setO_name(jsonObject.getString("ownername"));
                        w_details.setEmail(jsonObject.getString("email"));
                        w_details.setMobileno(jsonObject.getString("phone"));
                        w_details.setType(jsonObject.getString("type"));
                        w_details.setStoreage(jsonObject.getString("space"));
                        w_details.setCost(jsonObject.getString("cost"));
                        w_details.setAddress(jsonObject.getString("address"));
                        w_details.setPincode(jsonObject.getString("pincode"));
                        w_details.setRatting(jsonObject.getString("ratting"));

                        listt.add(w_details);

                        warehouseList.add(warehouse);


                    }

                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_LONG).show();
                error.printStackTrace();
                Log.i("error", "error is" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("phone", "" + user.getMobileno());
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void recyclerv() {
        recyclerView = findViewById(R.id.RecyclerView);

        mAdapter = new warehouseAdapter(warehouseList);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);

        //TODO OPEN NEW INTENT WITH PUT EXTRA FOR DETAILS OF WAREHOUSE

        recyclerView.addOnItemTouchListener(new RecyclerTouchListner(getApplicationContext(), recyclerView, new RecyclerTouchListner.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                warehouse warehouse = warehouseList.get(position);
                w_details wDetails = listt.get(position);
                Intent intent = new Intent(getApplicationContext(), warehouse_details.class);
                intent.putExtra("warehouse", wDetails);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        backend();
    }

    public void notification() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_OWNER_WARELIST

                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray obj = new JSONArray(response);


                    for (int i = 0; i < obj.length(); i++) {
                        JSONObject jsonObject = obj.getJSONObject(i);

                    }

                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_LONG).show();
                error.printStackTrace();
                Log.i("error", "error is" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("phone", "" + user.getMobileno());
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

}
