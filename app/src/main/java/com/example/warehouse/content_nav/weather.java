package com.example.warehouse.content_nav;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.R;
import com.example.warehouse.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class weather extends AppCompatActivity {

//    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, updatedField;

    EditText city;
    TextView weather;
    String cityname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        city = findViewById(R.id.city);
        weather = findViewById(R.id.weather);

//        Toast.makeText(getApplicationContext(), "weather called", Toast.LENGTH_SHORT).show();
//
//        cityField = findViewById(R.id.city_field);
//        updatedField = findViewById(R.id.updated_field);
//        detailsField = findViewById(R.id.details_field);
//        currentTemperatureField = findViewById(R.id.current_temperature_field);
//        humidity_field = findViewById(R.id.humidity_field);
//        pressure_field = findViewById(R.id.pressure_field);
//
//
//        weather_async.placeIdTask asyncTask = new weather_async.placeIdTask(new weather_async.AsyncResponse() {
//            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn) {
//                Toast.makeText(getApplicationContext(), "weathe_city" + weather_city, Toast.LENGTH_SHORT).show();
//                cityField.setText(weather_city);
//                updatedField.setText(weather_updatedOn);
//                detailsField.setText(weather_description);
//                currentTemperatureField.setText(weather_temperature);
//                humidity_field.setText("Humidity: " + weather_humidity);
//                pressure_field.setText("Pressure: " + weather_pressure);
//            }
//        });
//        asyncTask.execute("22.339430", "87.325340"); //  asyncTask.execute("Latitude", "Longitude")

    }

    public void weather(View view) {
        cityname = city.getText().toString();

//        DownTask downTask = new DownTask();
        backend();
//        downTask.execute("api.openweathermap.org/data/2.5/weather?q=" + cityname + "&appid=77d336b300c0aecaaa5c4747abfd8d19");
        weather.setVisibility(View.VISIBLE);
        Log.i("city name", cityname);
    }

    private void backend() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://api.openweathermap.org/data/2.5/weather?q=" + cityname + "&appid=77d336b300c0aecaaa5c4747abfd8d19",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.i("tag", "" + result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            String str = jsonObject.getString("weather");
                            Log.i("Progress", result);
                            JSONArray array = new JSONArray(str);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonpart = array.getJSONObject(i);

                                weather.setText("Main" + jsonpart.getString("main") + "\ndescription" + jsonpart.getString("description") + "\n icon " + jsonpart.getString("icon"));

                                Log.i("Main", jsonpart.getString("main"));
                                Log.i("description", jsonpart.getString("description"));
                                Log.i("icon", jsonpart.getString("icon"));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("Progress", "ERROR");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        Log.wtf("wtf", "" + error);
                    }
                });

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    public class DownTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(inputStream);

                int data = reader.read();
                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();

                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            return "null";
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String str = jsonObject.getString("weather");
                Log.i("Progress", result);
                JSONArray array = new JSONArray(str);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonpart = array.getJSONObject(i);

                    weather.setText("Main" + jsonpart.getString("main") + "\ndescription" + jsonpart.getString("description") + "\n icon " + jsonpart.getString("icon"));

                    Log.i("Main", jsonpart.getString("main"));
                    Log.i("description", jsonpart.getString("description"));
                    Log.i("icon", jsonpart.getString("icon"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Log.i("Progress", "ERROR");
            }
            super.onPostExecute(result);

        }
    }
}
