package com.example.warehouse.content_nav;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.widget.TextView;

import com.example.warehouse.R;

public class contactus extends AppCompatActivity {
    public static final String EMAIL_ADDRESSES = "codytark.lnct@gmail.com";
    TextView tv;
    String PHONE_NUMBERS = "1800-200-6000";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        tv = findViewById(R.id.contact);
        tv.setAutoLinkMask(Linkify.PHONE_NUMBERS | Linkify.EMAIL_ADDRESSES);
        //tv.setAutoLinkMask();
        tv.setText("Helpline Number=" + PHONE_NUMBERS + "\nEmail us:" + EMAIL_ADDRESSES);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

}
