package com.example.warehouse.content_nav;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warehouse.R;
import com.example.warehouse.SharedPref;
import com.example.warehouse.payment;
import com.example.warehouse.warehouse_dao.User;

public class warehouse_details extends AppCompatActivity {
    TextView reg;
    TextView ware;
    TextView owner;
    TextView email;
    TextView phone;
    TextView type;
    TextView storage;
    TextView cost;
    TextView address;
    TextView pincode;
    TextView ratting;
    User user;
    Button btn;

    com.example.warehouse.warehouse_dao.w_details w_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.warehouse_detail);
        w_details = (com.example.warehouse.warehouse_dao.w_details) getIntent().getSerializableExtra("warehouse");

        user = SharedPref.getInstance(getApplicationContext()).getUser();

        Toast.makeText(getApplicationContext(), w_details.getO_name() + " Welcomes you!", Toast.LENGTH_SHORT).show();
        find();
        set();

        if (user.getType().equals("user"))
            btn.setText("BOOK NOW");
        else
            btn.setText("Edit");

    }

    private void set() {
        reg.setText(w_details.getRegno());
        ware.setText(w_details.getWare_name());
        owner.setText(w_details.getO_name());
        email.setText(w_details.getEmail());
        phone.setText(w_details.getMobileno());
        type.setText(w_details.getType());
        storage.setText(w_details.getStoreage());
        cost.setText(w_details.getCost());
        address.setText(w_details.getAddress());
        pincode.setText(w_details.getPincode());
        ratting.setText(w_details.getRatting());
    }

    public void find() {
        reg = findViewById(R.id.registratiion2);
        ware = findViewById(R.id.ware2);
        owner = findViewById(R.id.owner2);
        email = findViewById(R.id.email2);
        phone = findViewById(R.id.mobile2);
        type = findViewById(R.id.type2);
        storage = findViewById(R.id.storage2);
        cost = findViewById(R.id.cost2);
        address = findViewById(R.id.address2);
        pincode = findViewById(R.id.pincode2);
        ratting = findViewById(R.id.ratting2);
        btn = findViewById(R.id.booknow);
    }

    public void bookno(View view) {
        if (user.getType().equals("user")) {
            {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                Toast.makeText(getApplicationContext(), "RESPONSE done", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), payment.class);
                        intent.putExtra("warehouse", w_details);
                        startActivity(intent);
                        finish();

                    }
                }, 2000);
            }

            Toast.makeText(getApplicationContext(), "Booking is progress... You will be contacted soon", Toast.LENGTH_SHORT).show();

        } else
            Toast.makeText(getApplicationContext(), "Please contact Admin For Changes", Toast.LENGTH_SHORT).show();

    }


}