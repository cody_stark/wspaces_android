package com.example.warehouse.content_nav;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.R;
import com.example.warehouse.SharedPref;
import com.example.warehouse.login_reg_pannels.LOGIN;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class editprofile extends AppCompatActivity {
    EditText mobile, fname, lname, opass, npass;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        user = SharedPref.getInstance(getApplicationContext()).getUser();
        find();

        if (!SharedPref.getInstance(getApplicationContext()).isLoggedIn()) {
            Toast.makeText(this, "Not Loggedin", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), LOGIN.class);
            startActivity(intent);
            finish();
        } else {
            backend1();
        }
    }

    private void backend1() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_PROFILE_EDIT
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //TODo:check the parsing exception prob

                try {
                    //converting response to json object
                    JSONObject obj = new JSONObject(response);
                    //if no error in response

                    if (!obj.getBoolean("error")) {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        //getting the user from the response
                        JSONObject userJson = obj.getJSONObject("user");

                        fname.setText(userJson.getString("fname"));
                        lname.setText(userJson.getString("lname"));


                        mobile.setText("" + user.getMobileno());
                        mobile.setEnabled(false);

                    } else {

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("mobileno", String.valueOf(user.getMobileno()).trim());
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    public void find() {
        fname = findViewById(R.id.FirstName);
        lname = findViewById(R.id.LastName);

        opass = findViewById(R.id.Passwordep);

        npass = findViewById(R.id.NewPassword);
        mobile = findViewById(R.id.Mobileet);
//        mobile.setText("756657589");

    }


    public void Update(View view) {
        backend2();
    }

    private void backend2() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_PROFILE_UPDATE
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);

                    //if no error in response
                    if (!obj.getBoolean("error")) {
                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        JSONObject userJson = obj.getJSONObject("user");

                        //creating a new user object
                        User user = new User(
                                Long.parseLong(userJson.getString("mobileno").toLowerCase()),
                                userJson.getString("fname").trim().toLowerCase(),
                                userJson.getString("email").trim().toLowerCase(),
                                userJson.getString("type").trim().toLowerCase()
                        );

                        SharedPref.getInstance(getApplicationContext()).userLogin(user);
                        finish();

//                        Toast.makeText(getApplicationContext(), obj.getString("TYPE")+userJson.getString("type"), Toast.LENGTH_SHORT).show();
//                        intentcall("user");
//                        intentcall(userJson.getString("type").trim().toLowerCase());


                    } else {

                        Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_SHORT).show();
                Log.i("editprofile::", "error" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("mobileno", mobile.getText().toString());
                params.put("fname", fname.getText().toString());
                params.put("lname", lname.getText().toString());
                params.put("opassword", opass.getText().toString());
                params.put("npassword", npass.getText().toString());
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }
}
