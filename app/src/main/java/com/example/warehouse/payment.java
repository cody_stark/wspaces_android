package com.example.warehouse;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warehouse.warehouse_dao.User;

public class payment extends AppCompatActivity {
    TextView reg;
    TextView storage;
    EditText cost1;
    User user;
    TextView phone;
    TextView Wname;

    com.example.warehouse.warehouse_dao.w_details w_details;
    String mod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Log.i("payment called","payment");
        user = SharedPref.getInstance(getApplicationContext()).getUser();

        w_details = (com.example.warehouse.warehouse_dao.w_details) getIntent().getSerializableExtra("warehouse");

        find();
        set();
    }

    private void set() {
        reg.setText(w_details.getRegno());
        storage.setText(w_details.getStoreage());
        phone.setText(String.valueOf(user.getMobileno()));
        cost1.setText("00");

    }

    private void find() {
        reg = findViewById(R.id.registratiion2);
        storage = findViewById(R.id.storagep);
        cost1 = findViewById(R.id.costp);
        phone = findViewById(R.id.mobilep);

        Wname = findViewById(R.id.warenamep);
    }

    public void Payment(View view) {
        Toast.makeText(this, "Payment Completed", Toast.LENGTH_SHORT).show();
        finish();
    }
}
