package com.example.warehouse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.login_reg_pannels.LOGIN;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;
import com.example.warehouse.warehouse_dao.w_details;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Ratting extends AppCompatActivity {
    RatingBar ratingBar;
    float rate;
    w_details w_details;
    User user;
    String feedback;
    EditText et;
    Button btn1, btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratting);
        ratingBar = findViewById(R.id.ratingBar);
        btn1 = findViewById(R.id.action);
        btn2 = findViewById(R.id.action2);
        et = findViewById(R.id.feedbacket);
        user = SharedPref.getInstance(getApplicationContext()).getUser();


        if (!SharedPref.getInstance(getApplicationContext()).isLoggedIn()) {

            Toast.makeText(this, "Not Logged in", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), LOGIN.class);
            startActivity(intent);
            finish();
        }
//        else {
////TODO :changed the backed from recyclev..check the connectivity
//
//
//        }


    }

    public void sub(View view) {
        rate = ratingBar.getRating();
        if (rate > 3) {
            feedback = "" + rate;
            backend();

        } else {
            et.setVisibility(View.VISIBLE);
            btn1.setVisibility(View.GONE);
            btn2.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "please give detailed feedback", Toast.LENGTH_LONG).show();
        }
    }

    private void backend() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_FEEDBACK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("tag", "" + response);
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (!obj.getBoolean("error")) {
                                Toast.makeText(getApplicationContext(), "Your Rating if Valueable", Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();
                                finish();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        Log.wtf("wtf", "" + error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rating", feedback);
                params.put("mobile", String.valueOf(user.getMobileno()));
                return params;
            }
        };

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    public void action2(View view) {
        feedback = "" + rate + "{\t" + et.getText() + "}";
        backend();
    }
}

