package com.example.warehouse.volley;

public class urls {
    private static final String ROOT_LOGIN_URL = "https://wspaces.000webhostapp.com/maintest/apicall.php?apicall=";
    public static final String URL_CONN = "https://wspaces.000webhostapp.com/maintest/conn.php";
    public static final String URL_REGISTER = ROOT_LOGIN_URL + "signup";
    public static final String URL_LOGIN = ROOT_LOGIN_URL + "login";
    public static final String URL_PROFILE_EDIT = ROOT_LOGIN_URL + "profile_edit";
    public static final String URL_PROFILE_UPDATE = ROOT_LOGIN_URL + "profile_update";
    public static final String URL_FEEDBACK = ROOT_LOGIN_URL + "feedback";

    private static final String ROOT_WHOUSE_URL = "https://wspaces.000webhostapp.com/maintest/warehouse.php?search=";
    public static final String URL_USER_WARELIST = ROOT_WHOUSE_URL + "userlog";
    public static final String URL_OWNER_WARELIST = ROOT_WHOUSE_URL + "warehouse_get";
    public static final String URL_OWNER_WARELIST_REGISTER = ROOT_WHOUSE_URL + "warehouse_reg";

    public static final String URL_INSERT_RATING = ROOT_WHOUSE_URL + "rating";
    private static final String ROOT_NOTIFICATION = "https://wspaces.000webhostapp.com/maintest/warehouse.php?search=";

    public static final String URL_OWNER_NOTIFIACTION = ROOT_NOTIFICATION + "";
    public static final String URL_USER_NOTIFIACTION = ROOT_NOTIFICATION + "";
}

