package com.example.warehouse;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.warehouse.RecyclerView.RecyclerTouchListner;
import com.example.warehouse.RecyclerView.warehouseAdapter;
import com.example.warehouse.content_nav.contactus;
import com.example.warehouse.content_nav.editprofile;
import com.example.warehouse.content_nav.explore;
import com.example.warehouse.content_nav.warehouse_details;
import com.example.warehouse.content_nav.weather;
import com.example.warehouse.login_reg_pannels.LOGIN;
import com.example.warehouse.volley.VolleySingleton;
import com.example.warehouse.volley.urls;
import com.example.warehouse.warehouse_dao.User;
import com.example.warehouse.warehouse_dao.w_details;
import com.example.warehouse.warehouse_dao.warehouse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User_nav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @SuppressLint("StaticFieldLeak")
    public static Context context;
    User user;
    View view;
    RelativeLayout mainLayout;
    Button btn;

    RatingBar ratingBar;
    float rate;
    w_details w_details;

    private List<warehouse> warehouseList = new ArrayList<>();
    private RecyclerView recyclerView;
    private warehouseAdapter mAdapter;
    EditText search1;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<w_details> listt = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_nav);
        user = SharedPref.getInstance(getApplicationContext()).getUser();
        other();

        if (!SharedPref.getInstance(getApplicationContext()).isLoggedIn()) {

            Toast.makeText(this, "Not Logged in", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), LOGIN.class);
            startActivity(intent);
            finish();
        } else {
//TODO :changed the backed from recyclev..check the connectivity
//            backend();
//            sec();
            recyclerv();

        }


    }

    private void sec() {
        search1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

    }

    private void filter(String txt) {
        ArrayList<warehouse> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (warehouse s : warehouseList) {

            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(txt) || s.getAddress().toLowerCase().contains(txt)) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

//        calling a method of the adapter class and passing the filtered list
        mAdapter.updateList(filterdNames);
    }

    public void other() {
        search1 = findViewById(R.id.Et_search);
        mainLayout = findViewById(R.id.rattingpannel);
        view = getLayoutInflater().inflate(R.layout.activity_ratting, mainLayout, false);

        ratingBar = findViewById(R.id.ratingBar);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile2) {
            Intent i = new Intent(getApplicationContext(), editprofile.class);
            startActivity(i);

            // Handle the camera action;
        } else if (id == R.id.nav_contact2) {
            Intent i = new Intent(getApplicationContext(), contactus.class);
            startActivity(i);


        } else if (id == R.id.nav_weather2) {
            Intent i = new Intent(getApplicationContext(), weather.class);
            startActivity(i);
        }


//    } else if (id == R.id.notification2) {
//        Intent i = new Intent(getApplicationContext(), Notification.class);
//        startActivity(i);
//    }
        else if (id == R.id.Rating2) {


//            mainLayout.addView(view);
//            btn=findViewById(R.id.action);
//            btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    rate = ratingBar.getRating();
//
////        backend();
//
//                    Toast.makeText(getApplicationContext(),""+rate,Toast.LENGTH_LONG).show();
//                }
//            });

            Intent i = new Intent(getApplicationContext(), Ratting.class);
            startActivity(i);
        } else if (id == R.id.nav_order) {
            Intent i = new Intent(getApplicationContext(), explore.class);
            startActivity(i);
        } else if (id == R.id.nav_logout2) {
            SharedPref.getInstance(getApplicationContext()).logout();

            Toast.makeText(this, "Logged Out successfull", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), LOGIN.class);
            startActivity(intent);
            finish();
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void backend() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, urls.URL_USER_WARELIST

                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray obj = new JSONArray(response);


                    for (int i = 0; i < obj.length(); i++) {
                        JSONObject jsonObject = obj.getJSONObject(i);

                        warehouse warehouse = new warehouse();
                        w_details w_details = new w_details();

                        warehouse.setName(jsonObject.getString("warehousename"));
                        warehouse.setOwner_name(jsonObject.getString("ownername"));
                        warehouse.setAddress(jsonObject.getString("address"));
                        warehouse.setRatting(jsonObject.getString("ratting"));

                        warehouseList.add(warehouse);

                        w_details.setRegno(jsonObject.getString("registration"));
                        w_details.setWare_name(jsonObject.getString("warehousename"));
                        w_details.setO_name(jsonObject.getString("ownername"));
                        w_details.setEmail(jsonObject.getString("email"));
                        w_details.setMobileno(jsonObject.getString("phone"));
                        w_details.setType(jsonObject.getString("type"));
                        w_details.setStoreage(jsonObject.getString("space"));
                        w_details.setCost(jsonObject.getString("cost"));
                        w_details.setAddress(jsonObject.getString("address"));
                        w_details.setPincode(jsonObject.getString("pincode"));
                        w_details.setRatting(jsonObject.getString("ratting"));

                        listt.add(w_details);
                    }
                    mAdapter.notifyDataSetChanged();
                    sec();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no Response" + error, Toast.LENGTH_LONG).show();
                error.printStackTrace();
                Log.i("error", "error is" + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                if (!search1.getText().toString().isEmpty())
                    params.put("Values", search1.getText().toString());
//                return super.getParams();

//            params.put("phone", "" + user.getMobileno());
                return params;
            }
        };
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void recyclerv() {
        recyclerView = findViewById(R.id.RecyclerView);

        mAdapter = new warehouseAdapter(warehouseList);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);
        //TODO OPEN NEW INTENT WITH PUT EXTRA FOR DETAILS OF WAREHOUSE
        recyclerView.addOnItemTouchListener(new RecyclerTouchListner(getApplicationContext(), recyclerView, new RecyclerTouchListner.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                warehouse warehouse = warehouseList.get(position);
                w_details wDetails = listt.get(position);


                Intent intent = new Intent(getApplicationContext(), warehouse_details.class);
                intent.putExtra("warehouse", wDetails);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        backend();
    }

    public void searchbtn(View view) {
//
//        Log.w("SEARCH BUTTON", "search1 value" + search1.getText().toString());
////        warehouseList.clear();
//        clear();
//        backend();
//
////        recyclerv();
    }

//    public void clear() {
//        int size = warehouseList.size();
//        warehouseList.clear();
//        mAdapter.notifyItemRangeRemoved(0, size);
//    }
}
